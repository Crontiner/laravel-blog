<?php

use Illuminate\Support\Facades\Mail;


Route::get('/post', function() {
	return view('single');
});


Route::get('/emailtest', function () {

	$data = [
				'title' => 'Email Title',
				'content' => 'lorem ipsum',
			];

	Mail::send('emails.index', $data, function($message){

		$message->to('paulovi91@gmail.com', 'Tibi')->subject('laravel test email');

	});

});



Route::get('404',['as'=>'404','uses'=>'ErrorHandlerController@errorCode404']);
Route::get('405',['as'=>'405','uses'=>'ErrorHandlerController@errorCode405']);


Route::get('/', 'PageController@front_page')->name('front-page');
Route::get('/blog', 'PageController@blog')->name('blog');
Route::get('/contact-us', 'PageController@contact_us')->name('contact-us');
Route::get('/about-us', 'PageController@about_us')->name('about-us');
Route::get('/login', 'PageController@login')->name('login');
Route::get('/registration', 'PageController@registration')->name('registration');


Route::group(['middleware' => 'admin'], function(){

	Route::get('/admin', 'AdminUsersController@front_page')->name('admin');
	Route::resource('admin/users', 'AdminUsersController');
	Route::resource('admin/posts', 'AdminPostsController');
	Route::resource('admin/posts-categories', 'AdminCategoriesController');
	Route::resource('admin/posts-tags', 'AdminTagsController');

});
Auth::routes();


/*
Route::namespace('Admin')->as('admin.')->middleware('admin')->group(function() {

	Route::get('/admin', 'AdminPageController@front_page')->name('admin');

});

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('admin/users', 'AdminUsersController');
*/
