<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Tag extends Model
{

	protected $fillable = ['name', 'slug', 'description'];


    public function post() {
		return $this->belongsTo('App\Post');
    }

    public function posts() {
		return $this->belongsToMany('App\Post', 'tags_posts');
    }

}