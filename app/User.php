<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'role_id', 'is_active', 'attachment_id',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];


    public function get_current_user_id() {
        return Auth::id();
    }

    public function role() {
        return $this->belongsTo('App\Role');
    }

    public function attachment() {
        return $this->belongsTo('App\Attachment');
    }

    public function posts() {
        return $this->hasMany('App\Post');
    }

    public function isAdmin() {
        if ( ($this->role->slug == 'administrator') && (intval($this->is_active) == 1) ) { return TRUE; }
        return FALSE;
    }

}
