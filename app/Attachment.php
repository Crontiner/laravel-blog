<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Routing\UrlGenerator;


class Attachment extends Model
{

	protected $fillable = ['file'];



	public function getMediaDirectoryUrl() {
		return url('images') .'/';
	}


	public function getMediaDirectoryPath() {
		return base_path() .'/images';
	}


	public function getFileAttribute($attachment) {
		return $this->getMediaDirectoryUrl() . $attachment;
	}


    public function posts() {
        return $this->hasMany('App\Post');
    }


    public function get_image_sizes() {

    	return array(
					'thumbnail' => array(
											'width' => 150,
											'height' => 150,
											'crop' => TRUE,
										),
					'medium' => array(
											'width' => 300,
											'height' => 300,
											'crop' => FALSE,
										),
					'medium_large' => array(
											'width' => 768,
											'height' => 0,
											'crop' => FALSE,
										),
					'large' => array(
											'width' => 1024,
											'height' => 1024,
											'crop' => FALSE,
										),
				);
    }

}