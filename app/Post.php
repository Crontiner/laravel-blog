<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Post extends Model
{

	protected $fillable = [
							'category_id',
							'tag_id',
							'user_id',
							'attachment_id',
							'title',
							'slug',
							'content',
							'excerpt',
							'status',
							'comment_status',
							'password',
							'date',
							'date_gmt',
						];


    public function user() {
		return $this->belongsTo('App\User');
    }

    public function attachment() {
		return $this->belongsTo('App\Attachment');
    }

    public function categories() {
		return $this->belongsToMany('App\Category', 'categories_posts');
    }

    public function tags() {
		return $this->belongsToMany('App\Tag', 'tags_posts');
    }

}