<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class AdminPageController extends Controller
{

    public function front_page() {
        return view('admin.index');
    }

}