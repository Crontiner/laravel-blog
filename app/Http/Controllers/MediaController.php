<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Attachment;
use Image;


class MediaController extends Controller
{


    // Save the original & resized images
    public function save_image($file = "") {
        $attachment = new Attachment();


        $filename_parts = pathinfo( $file->getClientOriginalName() ); // Original filename: imagename.jpg
        $filename = $filename_parts['filename']; // imagename
        $file_extension = $filename_parts['extension']; // jpg
        $sanitize_filename = $this->slug( $filename );

        $original_file_url = $attachment->getMediaDirectoryUrl() . $sanitize_filename .'.'. $file_extension;
        $original_file_path = $attachment->getMediaDirectoryPath() .'/'. $sanitize_filename .'.'. $file_extension;


        if ( file_exists($original_file_path) ) {
            // Add a serial number to the end of the filename


            // Grab last number in a string
            /*
            if(preg_match_all('/\d+/', $filename, $numbers)) {
                $serial_num = end($numbers[0]);
                                var_dump($serial_num); die;
                $serial_num = str_replace('-', "", $serial_num);
                $serial_num = intval($serial_num) + 1;
            } else { $serial_num = 2; }
            */

            $serial_num = $this->get_media_serial_hash();


            $filename_parts['filename'] = $filename .'-'. $serial_num;
            $filename_parts['basename'] = str_replace($filename, $filename .'-'. $serial_num, $filename_parts['basename']);

            // -- re-config

            $filename = $filename_parts['filename']; // imagename
            $file_extension = $filename_parts['extension']; // jpg
            $sanitize_filename = $this->slug( $filename );

            $original_file_url = $attachment->getMediaDirectoryUrl() . $sanitize_filename .'.'. $file_extension;
            $original_file_path = $attachment->getMediaDirectoryPath() .'/'. $sanitize_filename .'.'. $file_extension;
        }


        // Save The Original Image
        $file->move($attachment->getMediaDirectoryPath(), $sanitize_filename .'.'. $file_extension);
        $attachment = Attachment::create(['file' => $sanitize_filename .'.'. $file_extension]);


        $original_file_parameters = getimagesize( $original_file_url );
        $original_file_width = $original_file_parameters[0];
        $original_file_height = $original_file_parameters[1];


        // Save image in multiple sizes
        foreach ($attachment->get_image_sizes() as $size_name => $size_data) {
            if ( ($original_file_width > $size_data['width']) && ($original_file_height > $size_data['height']) ) {
                
                $resized_img_name = $sanitize_filename .'-';
                if ( $size_data['width'] > 0 ) { $resized_img_name .= $size_data['width']; }
                if ( ($size_data['width'] > 0) && ($size_data['height'] > 0) ) { $resized_img_name .= 'x'; }
                if ( $size_data['height'] > 0 ) { $resized_img_name .= $size_data['height']; }


                if ( $size_data['width'] > 0 ) {  } else { $size_data['width'] = NULL; }
                if ( $size_data['height'] > 0 ) {  } else { $size_data['height'] = NULL; }


                $save_file_full_path = $attachment->getMediaDirectoryPath() .'/'. $resized_img_name .'.'. $file_extension;


                if ( $size_data['crop'] === TRUE ) {

                    if ( intval($size_data['width']) < intval($size_data['height']) ) {
                        Image::make($original_file_url)->resize(NULL, $size_data['height'], function($constraint){
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        })->crop($size_data['width'], $size_data['height'])->save($save_file_full_path);                        
                    } else {
                        Image::make($original_file_url)->resize($size_data['width'], NULL, function($constraint){
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        })->crop($size_data['width'], $size_data['height'])->save($save_file_full_path);
                    }

                } else {
                    Image::make($original_file_url)->resize($size_data['width'], $size_data['height'], function($constraint){
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })->save($save_file_full_path);
                }
            }
        }


        return array('attachment' => $attachment);
    }


    public function get_media_serial_hash() {
        $hash = strtolower(md5(rand(1,9999)));
        return substr($hash, 0, 8);
    }


}