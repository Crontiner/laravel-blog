<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\UsersEditRequest;
use App\Http\Requests\UsersRequest;
use App\Attachment;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;


class AdminUsersController extends Controller
{

    public function front_page() {
        return view('admin.index');
    }

    public function index() {
        $users = User::paginate(10);

        return view('admin.users.index', compact('users'));
    }

    public function create() {
        $roles = Role::pluck('name','id')->all();

        return view('admin.users.create', compact('roles'));
    }

    public function store(UsersRequest $request) {
        $inputs = $request->all();


        if ( trim($request->password) == "" ) {
            $inputs = $request->except('password');
        } else {
            $inputs = $request->all();
            $inputs['password'] = Hash::make($request->password);
        }


        if ( $file = $request->file('attachment_id') ) {
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $attachment = Attachment::create(['file' => $name]);

            $inputs['attachment_id'] = $attachment->id;
        }


        User::create($inputs);
        return redirect('/admin/users');
    }

    public function show($id) {
        return view('admin.users.show');
    }

    public function edit($id) {
        $user = User::findOrFail($id);
        $roles = Role::pluck('name', 'id')->all();

        return view('admin.users.edit', compact('user', 'roles'));
    }

    public function update(UsersEditRequest $request, $id) {
        $user = User::findOrFail($id);


        if ( trim($request->password) == "" ) {
            $inputs = $request->except('password');
        } else {
            $inputs = $request->all();
            $inputs['password'] = Hash::make($request->password);
        }


        if ( $file = $request->file('attachment_id') ) {
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $attachment = Attachment::create(['file' => $name]);
            $inputs['attachment_id'] = $attachment->id;
        }


        $user->update($inputs);

        return redirect('/admin/users');
    }

    public function destroy($id) {
        $user = User::findOrFail($id);

        if ( isset($user->attachment->file) ) {
            $file_path = base_path('images') .'/'. basename($user->attachment->file);

            if ( unlink($file_path) ) {
                //Session::flash('deleted_user_image', 'The user\'s photo has been deleted');
            } else {
                Session::flash('deleted_user_image', 'The user\'s photo has not been deleted');
            }
        }

        $user->delete();
        Session::flash('deleted_user', 'The user has been deleted');

        return redirect('/admin/users');
    }

}