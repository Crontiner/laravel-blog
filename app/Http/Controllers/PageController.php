<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{

    public function blog() {
        return view('home');
    }


    public function contact_us() {
        return view('page');
    }


    public function about_us() {
        return view('page');
    }


    public function front_page() {
        return view('page');
    }


    public function login() {
        return view('auth.login');
    }

    public function registration() {
        return view('auth.register');
    }

}
