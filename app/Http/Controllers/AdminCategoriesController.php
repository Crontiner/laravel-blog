<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CategoriesEditRequest;
use App\Post;
use App\Category;


class AdminCategoriesController extends Controller
{

    public function index() {
        $categories = Category::orderBy('name', 'ASC')->paginate(10);

        return view('admin.posts-categories.index', compact('categories'));
    }

    public function create() {

    }

    public function store(CategoriesEditRequest $request) {
        $inputs = $request->all();

        if ( !isset($request->slug) ) {
            $inputs['slug'] = $this->slug($request->name);
        } else {
            $inputs['slug'] = $this->slug($request->slug);
        }

        Category::create($inputs);

        return redirect('/admin/posts-categories');
    }

    public function show($id) {

    }

    public function edit($id) {
        $category = Category::findOrFail($id);

        return view('admin.posts-categories.edit', compact('category'));
    }

    public function update(CategoriesEditRequest $request, $id) {
        $category = Category::findOrFail($id);
        $inputs = $request->all();

        if ( !isset($request->slug) ) {
            $inputs['slug'] = $this->slug($request->name);
        } else {
            $inputs['slug'] = $this->slug($request->slug);
        }

        $category->update($inputs);

        return redirect('/admin/posts-categories');
    }

    public function destroy($id) {

    }

}