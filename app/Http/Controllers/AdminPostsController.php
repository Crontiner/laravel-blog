<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\PostsEditRequest;
use App\Attachment;
use App\Post;
use App\User;
use App\Category;
use App\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\MediaController;


class AdminPostsController extends Controller
{

    protected $mediaController;



    public function __construct(MediaController $mediaController) {
        $this->mediaController = $mediaController;
    }


    public function index() {
        $posts = Post::orderBy('id', 'DESC')->paginate(10);

        return view('admin.posts.index', compact('posts'));
    }


    public function create() {
        $categories = Category::all();
        $tags = Tag::all();

        return view('admin.posts.create', compact('categories', 'tags'));
    }


    public function store(PostsEditRequest $request) {
        $user = new User();
        $inputs = $request->all();


        $inputs['user_id'] = $user->get_current_user_id();
        $inputs['attachment_id'] = 0;
        $inputs['title'] = $inputs['title'];
        $inputs['content'] = $inputs['content'];
        $inputs['excerpt'] = $inputs['excerpt'];
        $inputs['status'] = "publish"; // publish || draft || private || trash
        $inputs['comment_status'] = "approved"; // approved || disabled
        $inputs['password'] = "";
        $inputs['date'] = date('Y-m-d H:i:s');
        $inputs['date_gmt'] = gmdate('Y-m-d H:i:s');


        // Set Post slug
        if ( isset($inputs['slug']) ) {
            $inputs['slug'] = $this->slug($inputs['slug']);
        } else {
            $inputs['slug'] = $this->slug($inputs['title']);
        }


        // Set Featured Image
        if ( $file = $request->file('attachment_id') ) {
            $response = $this->mediaController->save_image($file);
            $inputs['attachment_id'] = $response['attachment']->id;
        }


        // Replace Default Publish Datetime
        if ( isset($inputs['publish_day']) && !empty($inputs['publish_day']) && 
             isset($inputs['publish_time']) && !empty($inputs['publish_time'])
            ) {
            $p_date = strtotime($inputs['publish_day'] .' '. $inputs['publish_time']);

            if ( checkdate(date('d', $p_date), date('m', $p_date), date('Y', $p_date)) ) {
                $inputs['date'] = date('Y-m-d H:i:s', $p_date);
                $inputs['date_gmt'] = gmdate('Y-m-d H:i:s', $p_date);
            }
        }


        if ( isset($response['attachment']) ) { 
            $new_post = Post::create($inputs, $response['attachment']); 
        } else { 
            $new_post = Post::create($inputs); 
        }


        // --- After the post is created ---

        if ( isset($new_post->id) ) {

            // Set Categories to post
            if ( isset($inputs['categories']) && !empty($inputs['categories']) ) {
                foreach ($inputs['categories'] as $category_id) {
                    $category = Category::find($category_id);
                    $created_data = $new_post->categories()->save($category);
                }
            }

            // Set Tags to post
            if ( isset($inputs['tags']) && !empty($inputs['tags']) ) {
                foreach ($inputs['tags'] as $tag_id) {
                    $tag = Tag::find($tag_id);
                    $created_data = $new_post->tags()->save($tag);
                }
            }

            return redirect('/admin/posts');
        }
    }


    public function show($id) {

    }


    public function edit($id) {
        $post = Post::findOrFail($id);

        return view('admin.posts.edit', compact('post'));
    }


    public function update(PostsEditRequest $request, $id) {
        $post = Post::findOrFail($id);
        $inputs = $request->all();
        $post->update($inputs);

        return redirect('/admin/posts');
    }


    public function destroy($id) {

    }

}
