<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TagsEditRequest;
use App\Post;
use App\Tag;


class AdminTagsController extends Controller
{

    public function index() {
        $tags = Tag::orderBy('name', 'ASC')->paginate(10);

        return view('admin.posts-tags.index', compact('tags'));
    }

    public function create() {

    }

    public function store(TagsEditRequest $request) {
        $inputs = $request->all();

        if ( !isset($request->slug) ) {
            $inputs['slug'] = $this->slug($request->name);
        } else {
            $inputs['slug'] = $this->slug($request->slug);
        }

        Tag::create($inputs);

        return redirect('/admin/posts-tags');
    }

    public function show($id) {

    }

    public function edit($id) {
        $tag = Tag::findOrFail($id);

        return view('admin.posts-tags.edit', compact('tag'));
    }

    public function update(TagsEditRequest $request, $id) {
        $tag = Tag::findOrFail($id);
        $inputs = $request->all();

        if ( !isset($request->slug) ) {
            $inputs['slug'] = $this->slug($request->name);
        } else {
            $inputs['slug'] = $this->slug($request->slug);
        }

        $tag->update($inputs);

        return redirect('/admin/posts-tags');
    }

    public function destroy($id) {

    }

}