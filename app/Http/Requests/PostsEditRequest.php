<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostsEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => '',
            'attachment_id' => '',
            'title' => 'required',
            'slug' => '',
            'content' => '',
            'excerpt' => '',
            'status' => '',
            'comment_status' => '',
            'password' => '',
        ];
    }
}
