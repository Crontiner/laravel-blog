@include('shared.header')

<!-- LOGIN FORM -->
<div class="login-container">
	<div class="row">
		<div class="small-12 large-7 large-centered medium-7 medium-centered columns">
			<div class="login-form">

				@if (session('status'))
				    <span class="invalid-feedback" role="alert">{{ session('status') }}</div>
				@else

					<form method="POST" action="{{ route('password.email') }}">
					    @csrf

						<fieldset class="large-12 cell">
							<label>{{ __('E-Mail Address') }}</label>
	                     	<input id="email" type="text" class="email {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

							@if ($errors->has('email'))
							    <span class="invalid-feedback" role="alert">
							        <strong>{{ $errors->first('email') }}</strong>
							    </span>
							@endif
						</fieldset>

						<fieldset class="large-12 cell">
							<button type="submit" class="button">{{ __('Send Password Reset Link') }}</button>
						</fieldset>
					</form>
				@endif

			</div>
		</div>
	</div>
</div>

@include('shared.footer')