@include('shared.header')

<!-- LOGIN FORM -->
<div class="login-container">
	<div class="row">
		<div class="small-12 large-7 large-centered medium-7 medium-centered columns">
			<div class="login-form">
                <form method="POST" action="{{ route('login') }}">
                    @csrf

					<fieldset class="large-12 cell">
						<label>{{ __('E-Mail Address') }}</label>
                     	<input id="email" type="text" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

						@if ($errors->has('email'))
						<div class="callout alert">
							<h5>{{ $errors->first('email') }}</h5>
						</div>
						@endif
					</fieldset>

					<fieldset class="large-12 cell">
						<label>{{ __('Password') }}</label> 
						<input id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

						@if ($errors->has('password'))
						    <div class="callout alert">
						        <h5>{{ $errors->first('password') }}</h5>
						    </div>
						@endif
					</fieldset>

					<fieldset class="large-12 cell">
						<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
						<label class="form-check-label" for="remember">{{ __('Remember Me') }}</label>
					</fieldset>

					<fieldset class="large-12 cell">
						<button type="submit" class="button">{{ __('Login') }}</button>
					</fieldset>

					<a class="hollow button small secondary" href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a>

                </form>
			</div>
		</div>
	</div>
</div>

@include('shared.footer')