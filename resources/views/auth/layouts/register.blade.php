@include('shared.header')

<!-- LOGIN FORM -->
<div class="login-container">
	<div class="row">
		<div class="small-12 large-7 large-centered medium-7 medium-centered columns">
			<div class="login-form">

                <form method="POST" action="{{ route('register') }}">
                    @csrf

                    <fieldset class="large-12 cell">
                        <label>{{ __('Name') }}</label>
                        <input id="name" type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </fieldset>

                    <fieldset class="large-12 cell">
                        <label>{{ __('E-Mail Address') }}</label>
                        <input id="email" type="text" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </fieldset>

                    <fieldset class="large-12 cell">
                        <label>{{ __('Password') }}</label>
                        <input id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </fieldset>

                    <fieldset class="large-12 cell">
                        <label>{{ __('Confirm Password') }}</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </fieldset>

                    <fieldset class="large-12 cell">
                        <button type="submit" class="button">{{ __('Register') }}</button>
                    </fieldset>
                </form>

			</div>
		</div>
	</div>
</div>

@include('shared.footer')