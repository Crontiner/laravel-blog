@include('shared.header')

<!-- LOGIN FORM -->
<div class="login-container">
	<div class="row">
		<div class="small-12 large-7 large-centered medium-7 medium-centered columns">
			<div class="login-form">

				<form method="POST" action="{{ route('password.update') }}">
				    @csrf
				    <input type="hidden" name="token" value="{{ $token }}">

				    <fieldset class="large-12 cell">
				        <label for="email" class="">{{ __('E-Mail Address') }}</label>
			            <input id="email" type="text" class="email {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>

			            @if ($errors->has('email'))
			                <span class="invalid-feedback" role="alert">
			                    <strong>{{ $errors->first('email') }}</strong>
			                </span>
			            @endif
				    </fieldset>

				    <fieldset class="large-12 cell">
				        <label for="password" class="">{{ __('Password') }}</label>
						<input id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

						@if ($errors->has('password'))
						    <span class="invalid-feedback" role="alert">
						        <strong>{{ $errors->first('password') }}</strong>
						    </span>
						@endif
				    </fieldset>

				    <fieldset class="large-12 cell">
				        <label>{{ __('Confirm Password') }}</label>
			            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
				    </fieldset>

				    <fieldset class="large-12 cell">
			            <button type="submit" class="button">{{ __('Reset Password') }}</button>
				    </fieldset>
				</form>

			</div>
		</div>
	</div>
</div>

@include('shared.footer')