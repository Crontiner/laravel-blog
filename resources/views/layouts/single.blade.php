@include('shared.header')

<!-- SINGLE POST CONTENT -->
<div class="sectionarea blog">
	<div class="row">
		<div class="small-12 large-12 medium-12 columns">

			<!-- POST START -->
			<div class="entry">
				<div class="meta">12th of August 2014, Written by <a href="">Stephen Leggings</a> <i class="fa fa-comment-o"></i><a href="">3 comments</a></div>
				<img src="images/blog1.jpg" alt=""/>
				<p>Freegan twee VHS, Carles iPhone chia Thundercats Godard meh photo booth gluten-free chambray. Brooklyn paleo distillery gluten-free. Selvage Thundercats Etsy post-ironic Echo Park vinyl. Wes Anderson before they sold out YOLO, leggings cardigan literally salvia master cleanse beard you probably haven't heard of them letterpress. Wolf High Life Banksy actually, salvia readymade cardigan farm-to-table mixtape. Portland selfies hashtag, hella tote bag scenester disrupt lo-fi artisan bespoke Neutra. Aesthetic slow-carb cardigan master cleanse polaroid hashtag.</p>
				<p>Synth qui locavore, paleo Intelligentsia wolf fugiat bicycle rights whatever kale chips vegan Shoreditch drinking vinegar skateboard. Mollit aliqua deserunt, squid bespoke normcore Marfa American Apparel tattooed occaecat duis. Banjo assumenda flexitarian Thundercats. Farm-to-table Truffaut delectus reprehenderit readymade. Cornhole pork belly ugh, wolf mlkshk enim occaecat pug jean shorts street art meggings church-key. PBR artisan placeat, Shoreditch you probably haven't heard of them vegan next level post-ironic fap selvage put a bird on it. Id voluptate do in dolor, nesciunt quis.</p>
				<p>VHS photo booth ut dreamcatcher. Nostrud non actually Blue Bottle butcher ad. Tumblr pork belly readymade Echo Park chillwave blog eiusmod. Brunch cardigan irony, mlkshk ugh mumblecore food truck next level nesciunt leggings fap. Asymmetrical locavore eu id commodo, aesthetic tempor banjo dolor. Food truck gastropub art party, literally fugiat fanny pack letterpress church-key. Slow-carb yr Truffaut master cleanse fugiat.</p>
				<p>Oh. You need a little dummy text for your mockup? How quaint.</p>
				<p>I bet you’re still using Bootstrap too…</p>
			</div>
			<!-- POST END -->

			<!-- SHARE -->
			<div class="single-section-container">
				<h6 class="single-section-title"><span class="single-section-text">Share</span></h6>
			</div>
			<div class="sharing-buttons">
				<ul>
					<li><a href="blog-single.html#" title="facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
					<li><a href="blog-single.html#" title="twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
					<li><a href="blog-single.html#" title="pinterest" target="_blank"><i class="fa fa-pinterest"></i></a></li>
					<li><a href="blog-single.html#" title="googleplus" target="_blank"><i class="fa fa-google-plus"></i></a></li>
					<li><a href="blog-single.html#" title="linkedin" target="_blank"><i class="fa fa-linkedin"></i></a></li>
					<li><a href="blog-single.html#" title="stumbleupon" target="_blank"><i class="fa fa-stumbleupon"></i></a></li>
					<li><a href="blog-single.html#" title="digg" target="_blank"><i class="fa fa-digg"></i></a></li>
				</ul>
			</div>
			<!-- END OF SHARE -->

			<!-- AUTHOR BOX -->
			<div class="author-wrap">
				<div class="row">
					<div class="small-12 large-2 medium-3 columns">
						<div class="author-gravatar"><img src="images/gravatar.png" alt=""/></div>
					</div>
					<div class="small-12 large-10 medium-9 columns">
						<div class="author-info">
							<div class="author author-title">
								<h6>audemedia</h6>
							</div>
							<div class="author-description">
								<p> Brunch cardigan irony, mlkshk ugh mumblecore food truck next level nesciunt leggings fap. Asymmetrical locavore eu id commodo, aesthetic tempor banjo dolor. Food truck gastropub art party. Slow-carb yr Truffaut master cleanse fugiat.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END OF AUTHOR BOX -->

			<!-- COMMENTS -->
			<div class="commentsform">
				<div id="addcomments">
					<div id="respond" class="comment-respond">
						<h3 id="reply-title" class="comment-reply-title">Leave a Reply</h3>
						<form action="blog_single.html" method="post" id="commentform" class="comment-form" novalidate>
							<p class="comment-notes">Your email address will not be published. Required fields are marked <span class="required">*</span></p>
							<p class="comment-form-author"><label for="author">Name <span class="required">*</span></label> <input id="author" name="author" type="text" value="" size="30" aria-required='true' /></p>
							<p class="comment-form-email"><label for="email">Email <span class="required">*</span></label> <input id="email" name="email" type="email" value="" size="30" aria-required='true' /></p>
							<p class="comment-form-url"><label for="url">Website</label> <input id="url" name="url" type="url" value="" size="30" /></p>
							<p class="comment-form-comment"><label for="comment">Comment</label> <textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>
							<p class="form-submit"><input name="submit" type="submit" id="submit" value="Post Comment" /></p>
						</form>
					</div>
				</div>
			</div>
			<!-- END OF COMMENTS -->
		</div>

	</div>
</div>
<!-- END OF CONTENT -->

@include('shared.footer')