@include('shared.header')

<!-- POSTS CONTENT -->
<div class="sectionarea blog">
    <div class="row">
        <div class="small-12 large-9 medium-9 columns">

            <!-- POST START -->
            <div class="entry">
                <h2><a href="">Asymmetrical locavore tote bag</a></h2>
                <img src="{{ asset('public/dist/img/blog1.jpg') }}" alt=""/>
                <p>Freegan twee VHS, Carles iPhone chia Thundercats Godard meh photo booth gluten-free chambray. Brooklyn paleo distillery gluten-free. Selvage Thundercats Etsy post-ironic Echo Park vinyl. Wes Anderson before they sold out YOLO, leggings cardigan literally salvia master cleanse beard you probably haven't heard of them letterpress. Wolf High Life Banksy actually, salvia readymade cardigan farm-to-table mixtape. Portland selfies hashtag, hella tote bag scenester disrupt lo-fi artisan bespoke Neutra. Aesthetic slow-carb cardigan master cleanse polaroid hashtag.</p>
                <a href="blog.html#" class="small radius button">read more</a>
                <div class="meta">12th of August 2014, Written by <a href="">Stephen Leggings</a> <i class="fa fa-comment-o"></i><a href="">3 comments</a></div>
            </div>
            <!-- POST END -->


            <!-- PAGINATION -->
            <ul class="pagination" role="navigation" aria-label="Pagination">
                <li class="disabled">Previous <span class="show-for-sr">page</span></li>
                <li class="current"><span class="show-for-sr">You're on page</span> 1</li>
                <li><a href="blog.html#" aria-label="Page 2">2</a></li>
                <li><a href="blog.html#" aria-label="Page 3">3</a></li>
                <li><a href="blog.html#" aria-label="Page 4">4</a></li>
                <li class="ellipsis" aria-hidden="true"></li>
                <li><a href="blog.html#" aria-label="Page 12">12</a></li>
                <li><a href="blog.html#" aria-label="Page 13">13</a></li>
                <li><a href="blog.html#" aria-label="Next page">Next <span class="show-for-sr">page</span></a></li>
            </ul>
            <!-- END OF PAGINATION -->

        </div>

        <div class="small-12 large-3 medium-3 columns">
            @include('shared.sidebar')
        </div>
    </div>
</div>
<!-- END OF CONTENT -->

@include('shared.footer')