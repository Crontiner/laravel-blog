@include('shared.header')

<!--  MAIN PAGE CONTENT  -->
<div class="about-us-subheader page-content">
	<div class="row">
		<div class="small-12 columns">
			<h2 class="post-title">The requested page could not be found.</h2>
		</div>
	</div>
</div>
<!-- END OF MAIN PAGE CONTENT  -->

@include('shared.footer')