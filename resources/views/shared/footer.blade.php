        <!--  FOOTER  -->
        <footer>
            <div class="row">
                <div class="small-12 columns">
                    <div class="footerlinks">
                        <div class="small-12 large-3 medium-3 columns border-right">
                            <h2>Hosting Services</h2>
                            <ul>
                                <li><a href="shared.html">Shared Hosting</a></li>
                                <li><a href="vps.html">Managed VPS</a></li>
                                <li><a href="servers.html">Dedicated Services</a></li>
                                <li><a href="">Become a Reseller</a></li>
                                <li><a href="">Special Offers</a></li>
                            </ul>
                        </div>
                        <div class="small-12 large-3 medium-3 columns border-right">
                            <h2>Company</h2>
                            <ul>
                                <li><a href="">About us</a></li>
                                <li><a href="">Blog</a></li>
                                <li><a href="">Terms of Service</a></li>
                                <li><a href="">Acceptable Use Policy</a></li>
                                <li><a href="">Affiliates</a></li>
                            </ul>
                        </div>
                        <div class="small-12 large-3 medium-3 columns border-right">
                            <h2>Add-on Services</h2>
                            <ul>
                                <li><a href="">SSL Certificates</a></li>
                                <li><a href="">Dedicated IPs</a></li>
                                <li><a href="">Control Panel Licenses</a></li>
                                <li><a href="">WHMCS License</a></li>
                                <li><a href="">Migrations / Transfers</a></li>
                            </ul>
                        </div>
                        <div class="small-12 large-3 medium-3 columns">
                            <h2>CloudMe Newsletter</h2>
                            <p>Sign up for special offers:</p>
                            <!-- Begin MailChimp Signup Form -->
                            <div id="mc_embed_signup">
                                <form action="//audemedia.us7.list-manage.com/subscribe/post?u=b5638e105dac814ad84960d90&amp;id=9345afa0aa" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                    <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
                                    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                    <div style="position: absolute; left: -5000px;"><input type="text" name="b_b5638e105dac814ad84960d90_9345afa0aa" tabindex="-1" value=""></div>
                                    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                                </form>
                            </div>
                        </div>
                        <!--End mc_embed_signup-->
                    </div>
                </div>
            </div>
            <p class="copyright">© Copyright CloudMe, all rights reserved. </p>
        </footer>
        <!--  END OF FOOTER  -->
        <a href="blog.html#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>
        <script src="{{ asset('public/js/vendor/jquery.js') }}"></script>
        <script src="{{ asset('public/js/vendor/what-input.min.js') }}"></script>
        <script src="{{ asset('public/js/foundation.min.js') }}"></script>
        <script src="{{ asset('public/js/vendor/hoverIntent.js') }}"></script>
        <script src="{{ asset('public/js/vendor/superfish.min.js') }}"></script>
        <script src="{{ asset('public/js/vendor/morphext.min.js') }}"></script>
        <script src="{{ asset('public/js/vendor/wow.min.js') }}"></script>
        <script src="{{ asset('public/js/vendor/jquery.slicknav.min.js') }}"></script>
        <script src="{{ asset('public/js/vendor/waypoints.min.js') }}"></script>
        <script src="{{ asset('public/js/vendor/jquery.animateNumber.min.js') }}"></script>
        <script src="{{ asset('public/js/vendor/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('public/js/vendor/retina.min.js') }}"></script>
        <script src="{{ asset('public/js/custom.js') }}"></script>
    </body>
</html>