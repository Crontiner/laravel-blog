<?php 

$segment = Request::segment(1); // "" || "categories" || "news"

?>
<!doctype html>
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <link rel="shortcut icon" href="images/icons/favicon.png" />
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="{{ asset('public/css/foundation.css') }}" />
        <link rel="stylesheet" href="{{ asset('public/css/font-awesome.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('public/css/animate.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('public/css/morphext.css') }}" />
        <link rel="stylesheet" href="{{ asset('public/css/owl.carousel.css') }}">
        <link rel="stylesheet" href="{{ asset('public/css/owl.theme.css') }}">
        <link rel="stylesheet" href="{{ asset('public/css/owl.transitions.css') }}">
        <link rel="stylesheet" href="{{ asset('public/css/slicknav.css') }}">
        <link rel="stylesheet" href="{{ asset('public/css/style.css') }}" />

        <!-- Custom Styles -->
        <link rel="stylesheet" href="{{ asset('public/css/custom-styles.css') }}" />

    </head>
    <body class="@yield('body_class')">

        <!--  HEADER -->
        <header class="alt-2 @yield('header_class')">
            <div class="top">
                <div class="row">
                    <div class="small-12 large-3 medium-3 columns">
                        <div class="logo">
                            <a href="{{ route('front-page') }}" title=""><img src="{{ asset('public/dist/img/logo.png') }}" alt="" title=""/></a>
                        </div>
                    </div>
                    <div class="small-12 large-9 medium-9 columns">
                        <!--  NAVIGATION MENU AREA -->
                        <nav class="desktop-menu">
                            <ul class="sf-menu" id="navigation">
                                <li class="@if($segment == '') current-menu-item @endif">
                                    <a href="{{ route('front-page') }}">Home</a>
                                </li>
                                <li class="@if($segment == 'about-us') current-menu-item @endif">
                                    <a href="{{ route('about-us') }}">About Us</a>
                                </li>
                                <li class="@if($segment == 'blog') current-menu-item @endif">
                                    <a href="{{ route('blog') }}">Blog</a>
                                </li>
                                <li class="@if($segment == 'contact-us') current-menu-item @endif">
                                    <a href="{{ route('contact-us') }}">Contact</a>
                                </li>

                                @guest
                                <li class="@if($segment == 'login') current-menu-item @endif">
                                    <a href="{{ route('login') }}">{{ __('Log in') }}</a>
                                </li>
                                <li class="@if($segment == 'registration') current-menu-item @endif">
                                    <a href="{{ route('registration') }}">{{ __('Sign up') }}</a>
                                </li>
                                @else
                                <li>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidden">
                                        @csrf
                                    </form>
                                </li>
                                <li>
                                    <a href="#">Hello, {{ Auth::user()->name }}!</a>
                                </li>
                                @endguest

                            </ul>
                        </nav>
                        <!--  END OF NAVIGATION MENU AREA -->
                    </div>
                </div>
            </div>
            <!--  MESSAGES ABOVE HEADER IMAGE -->
            <div class="message">
                <div class="row">
                    <div class="small-12 columns">
                        <div class="message-intro">
                            <span class="message-line"></span>
                            <p>Blog</p>
                            <span class="message-line"></span>
                        </div>
                        <h1>THE LATEST CLOUDME NEWS</h1>
                    </div>
                </div>
            </div>
            <!--  END OF MESSAGES ABOVE HEADER IMAGE -->
        </header>
        <!--  END OF HEADER -->