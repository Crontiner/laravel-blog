<!-- SIDEBAR -->
<div class="sidebar">

    <div class="widget">
        <h4 class="widget_title"><span>Categories</span></h4>
        <ul>
            <li><a href="" title="">Managed Hosting</a></li>
            <li><a href="" title="">Dedicated Servers</a></li>
            <li><a href="" title="">Marketing</a></li>
            <li><a href="" title="">Customer Service</a></li>
            <li><a href="" title="">Infrastructure</a></li>
            <li><a href="" title="">Announcements</a></li>
            <li><a href="" title="">Server Performance</a></li>
        </ul>
    </div>

    <div class="widget">
        <h4 class="widget_title"><span>Search</span></h4>
        <form method="get" id="searchform" action="blog_category.html">
            <div class="row collapse">
                <div class="small-11 large-11 medium-11 columns">
                    <input type="text" name="s" id="s" value="">
                </div>
                <div class="small-1 large-1 medium-1 columns">
                    <button type="submit" class="button tiny"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </form>
    </div>

    <div class="widget">
        <h4 class="widget_title"><span>ADVERTISEMENT</span></h4>
        <div class="textwidget">
            <a href="http://themeforest.net/?ref=audemedia" target="_blank"><img src="{{ asset('public/dist/img/tf_300x250_v5.gif') }}" alt="ad banner"/></a>
        </div>
    </div>

    <div class="widget">
        <h4 class="widget_title"><span>TAGS</span></h4>
        <div class="tagcloud">
            <a href="">web servers</a>
            <a href="">windows</a>
            <a href="">unix</a>
            <a href="">server solutions</a>
            <a href="">colocation</a>
            <a href="">data centers</a>
            <a href="">server hardware</a>
            <a href="">bandwidth</a>
            <a href="">web hosting</a>
            <a href="">cloud hosting</a>
            <a href="">internet</a>
            <a href="">game-servers</a>
        </div>
    </div>

</div>
<!-- END OF SIDEBAR -->