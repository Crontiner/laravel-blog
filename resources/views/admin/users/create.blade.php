@extends('admin.layouts.index')

@section('page_title', 'Create Users')
@section('body_class', 'users-page create')

@section('content')

	{!! Form::open([
					'method' => 'POST', 
					'action' => 'AdminUsersController@store', 
					'class' => 'col-md-6',
					'files' => TRUE,
				]) 
	!!}

		<div class="form-group">
			{!! Form::label('name', 'Name:') !!}
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('email', 'Email:') !!}
			{!! Form::email('email', null, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('role_id', 'Role:') !!}
			{!! Form::select('role_id', ['' => 'Choose Options'] + $roles, 0, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('is_active', 'Status:') !!}
			{!! Form::select('is_active', array(1 => 'Active', 0 => 'Not Active'), 0, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('attachment_id', 'Photo:') !!}<br>
			{!! Form::file('attachment_id', null, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('password', 'Password:') !!}
			{!! Form::password('password', ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::submit('Create User', ['class' => 'btn btn-primary']) !!}
		</div>

	{!! Form::close() !!}


	@include('admin.layouts.form_error')


@endsection