@extends('admin.layouts.index')

@section('page_title', 'Users')
@section('body_class', 'users-page index')

@section('content')


<div class="card">
	<div class="card-header hide">
		<h3 class="card-title">Hover Data Table</h3>
	</div>
	<!-- /.card-header -->
	<div class="card-body">
		<div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
			<div class="row">
				<div class="col-sm-12 col-md-6"></div>
				<div class="col-sm-12 col-md-6"></div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
						<thead>
							<tr role="row">
								<th>ID</th>
								<th>Photo</th>
								<th>Name</th>
								<th>Role</th>
								<th>Status</th>
								<th>Emails</th>
								<th>Created</th>
								<th>Updated</th>
							</tr>
						</thead>
						<tbody>

							@if($users)
								@foreach($users as $user)
									<tr>
										<td>{{ $user->id }}</td>
										<td>
											<img src="{{ $user->attachment ? $user->attachment->file : 'https://dummyimage.com/200x200/' }}" alt="" width="100" class="img-responsive img-rounded" />
										</td>
										<td>
											<a href="{{ route('users.edit', $user->id) }}">{{ $user->name }}</a>
										</td>
										<td>{{ $user->role->name }}</td>
										<td>{{ $user->is_active == 1 ? 'Active' : 'Not Active' }}</td>
										<td>{{ $user->email }}</td>
										<td>{{ $user->created_at }} <small>({{ $user->created_at->diffForHumans() }})</small></td>
										<td>{{ $user->updated_at }} <small>({{ $user->updated_at->diffForHumans() }})</small></td>
									</tr>
								@endforeach
							@endif

						</tbody>
					</table>
				</div>
			</div>

			@if ( $users->perPage() < $users->total() )
			<div class="row">
				<div class="col-sm-12 col-md-5">
					<div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing 1 to {{ $users->perPage() }} of {{ $users->total() }} entries</div>
				</div>
				<div class="col-sm-12 col-md-7">
					<div class="dataTables_paginate paging_simple_numbers pull-right" id="example2_paginate">
						{{ $users->render() }}
					</div>
				</div>
			</div>
			@endif

		</div>
	</div>
	<!-- /.card-body -->
</div>

@endsection