@extends('admin.layouts.index')

@section('page_title', 'Edit Users')
@section('body_class', 'users-page edit')

@section('content')

	
	@if($user->attachment)
	<div class="col-md-6 col-sm-3">
		<img src="{{ $user->attachment->file }}" alt="" class="img-responsive img-rounded profile-img" />
	</div>
	@endif


	{!! Form::model($user, [
					'method' => 'PATCH', 
					'action' => ['AdminUsersController@update', $user->id], 
					'class' => 'col-md-6',
					'files' => TRUE,
				]) 
	!!}

		<div class="form-group">
			{!! Form::label('name', 'Name:') !!}
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('email', 'Email:') !!}
			{!! Form::email('email', null, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('role_id', 'Role:') !!}
			{!! Form::select('role_id', $roles, 0, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('is_active', 'Status:') !!}
			{!! Form::select('is_active', array(1 => 'Active', 0 => 'Not Active'), $user->is_active, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('attachment_id', 'Photo:') !!}<br>
			{!! Form::file('attachment_id', null, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('password', 'Password:') !!}
			{!! Form::password('password', ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::submit('Update User', ['class' => 'btn btn-primary']) !!}
		</div>

	{!! Form::close() !!}



	{!! Form::model($user, [
					'method' => 'DELETE',
					'action' => ['AdminUsersController@destroy', $user->id], 
					'class' => 'col-md-6',
					'files' => TRUE,
				]) 
	!!}

		<div class="form-group">
			{!! Form::submit('Delete User', ['class' => 'btn btn-danger']) !!}
		</div>

	{!! Form::close() !!}


	@include('admin.layouts.form_error')


@endsection