@extends('admin.layouts.index')

@section('page_title', 'Posts Tags')
@section('body_class', 'posts-tags index')

@section('content')


<div class="row">
	<div class="col-md-6">

		<div class="card card-primary">
			<div class="card-header">
				<h3 class="card-title">Add New Tag</h3>
			</div>

			{!! Form::open([
							'method' => 'POST', 
							'action' => 'AdminTagsController@store',
							'files' => TRUE,
						])
			!!}
				<div class="card-body">
					
					<div class="form-group">
						{!! Form::label('name', 'Name:') !!}
						{!! Form::text('name', null, ['class' => 'form-control']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('slug', 'Slug:') !!}
						{!! Form::text('slug', null, ['class' => 'form-control']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('description', 'Description:') !!}
						{!! Form::textarea('description', null, ['class' => 'form-control']) !!}
					</div>

					<div class="form-group">
						{!! Form::submit('Add New Tag', ['class' => 'btn btn-primary']) !!}
					</div>

				</div>
			{!! Form::close() !!}

			@include('admin.layouts.form_error')
		</div>

	</div>
	<div class="col-md-6">

		<div class="card">
			<!-- /.card-header -->
			<div class="card-body">
				<div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
					<div class="row">
						<div class="col-sm-12 col-md-6"></div>
						<div class="col-sm-12 col-md-6"></div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
								<thead>
									<tr role="row">
										<th>Name</th>
										<th>Description</th>
										<th>Slug</th>
										<th>Post Count</th>
									</tr>
								</thead>
								<tbody>

								@if($tags)
									@foreach($tags as $tag)
										<tr>
											<td>
												<a href="{{ route('posts-tags.edit', $tag->id) }}">{{ $tag->name }}</a>
											</td>
											<td>{{ $tag->description }}</td>
											<td>{{ $tag->slug }}</td>
											<td>{{ $tag->posts->count() }}</td>
										</tr>
									@endforeach
								@endif

								</tbody>
							</table>
						</div>
					</div>

					@if ( $tags->perPage() < $tags->total() )
					<div class="row">
						<div class="col-sm-12 col-md-5">
							<div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing 1 to {{ $tags->perPage() }} of {{ $tags->total() }} entries</div>
						</div>
						<div class="col-sm-12 col-md-7">
							<div class="dataTables_paginate paging_simple_numbers pull-right">
								{{ $tags->render() }}
							</div>
						</div>
					</div>
					@endif

				</div>
			</div>
			<!-- /.card-body -->
		</div>

	</div>
</div>


@endsection