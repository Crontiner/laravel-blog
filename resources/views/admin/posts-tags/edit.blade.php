@extends('admin.layouts.index')

@section('page_title', 'Edit Tag')
@section('body_class', 'posts-tags edit')

@section('content')


<div class="row">
	<div class="col-md-6">

		<div class="card card-primary">
			<div class="card-header">
				<h3 class="card-title">Edit Tag</h3>
			</div>

			{!! Form::model($tag, [
							'method' => 'PATCH', 
							'action' => ['AdminTagsController@update', $tag->id], 
							'files' => TRUE,
						]) 
			!!}
				<div class="card-body">
					
					<div class="form-group">
						{!! Form::label('name', 'Name:') !!}
						{!! Form::text('name', null, ['class' => 'form-control']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('slug', 'Slug:') !!}
						{!! Form::text('slug', null, ['class' => 'form-control']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('description', 'Description:') !!}
						{!! Form::textarea('description', null, ['class' => 'form-control']) !!}
					</div>

					<div class="form-group">
						{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
					</div>

				</div>
			{!! Form::close() !!}

			@include('admin.layouts.form_error')
		</div>

	</div>
	<div class="col-md-6"></div>
</div>


@endsection