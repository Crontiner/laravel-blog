@extends('admin.layouts.index')

@section('page_title', 'Create Post')
@section('body_class', 'posts-page create')

@section('content')


	{!! Form::open([
					'method' => 'POST', 
					'action' => 'AdminPostsController@store', 
					'class' => 'row',
					'files' => TRUE,
				]) 
	!!}


		<div class="col-md-6">
			<div class="card card-primary">
				<div class="card-body">
					<div class="form-group">
						{!! Form::label('title', 'Title:') !!}
						{!! Form::text('title', null, ['class' => 'form-control']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('slug', 'Slug:') !!}
						{!! Form::text('slug', null, ['class' => 'form-control']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('content', 'Content:') !!}
						{!! Form::textarea('content', null, ['class' => 'form-control']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('excerpt', 'Excerpt:') !!}
						{!! Form::textarea('excerpt', null, ['class' => 'form-control']) !!}
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-6">

			<div class="row">
				<div class="col-md-6">
					<div class="card card-primary">
						<div class="card-header">
							<h3 class="card-title">Categories</h3>
						</div>
						<div class="card-body">
							<div class="form-group">
								@foreach($categories as $c)
									<div class="form-check">
										{{ Form::checkbox('categories[]', $c->id, false, ['class' => 'form-check-input', 'id' => $c->slug]) }}
										<label class="form-check-label" for="{{ $c->slug }}">{{ $c->name }}</label>
									</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="card card-primary">
						<div class="card-header">
							<h3 class="card-title">Tags</h3>
						</div>
						<div class="card-body">
							<div class="form-group">
								@foreach($tags as $t)
									<div class="form-check">
										{{ Form::checkbox('tags[]', $t->id, false, ['class' => 'form-check-input', 'id' => $t->slug]) }}
										<label class="form-check-label" for="{{ $t->slug }}">{{ $t->name }}</label>
									</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="card card-primary">
						<div class="card-header">
							<h3 class="card-title">Featured Image</h3>
						</div>
						<div class="card-body">
							<div class="form-group">
								{!! Form::file('attachment_id', ['class' => 'form-control']) !!}
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="card card-primary">
						<div class="card-header">
							<h3 class="card-title">Publish</h3>
						</div>
						<div class="card-body">

							<div class="form-group">
								<label>Publish date:</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="text" name="publish_day" class="form-control" value="{{ date('Y-m-d') }}" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask="">
									<input type="text" name="publish_time" class="form-control timepicker" value="{{ date('H:i:s') }}">
								</div>
							</div>

						</div>
						<div class="card-footer">
							{!! Form::submit('Publish', ['class' => 'btn btn-primary']) !!}
						</div>
					</div>
				</div>
			</div>

		</div>


	{!! Form::close() !!}


	@include('admin.layouts.form_error')


@endsection