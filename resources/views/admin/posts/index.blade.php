@extends('admin.layouts.index')

@section('page_title', 'Posts')
@section('body_class', 'posts-page index')

@section('content')


<div class="card">
	<div class="card-header hide">
		<h3 class="card-title">Hover Data Table</h3>
	</div>
	<!-- /.card-header -->
	<div class="card-body">
		<div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
			<div class="row">
				<div class="col-sm-12 col-md-6"></div>
				<div class="col-sm-12 col-md-6"></div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
						<thead>
							<tr role="row">
								<th>Title</th>
								<th>Author</th>
								<th>Categories</th>
								<th>Tags</th>
								<th>Featured Image</th>
								<th>Created</th>
							</tr>
						</thead>
						<tbody>

							@if($posts)
								@foreach($posts as $post)
									<tr data-post-id="{{ $post->id }}">
										<td>
											<a href="{{ route('posts.edit', $post->id) }}">{{ $post->title }}</a>
										</td>
										<td>
											<a href="{{ route('users.edit', $post->user->id) }}">{{ $post->user->name }}</a>
										</td>
										<td>
											@if($post->categories)
											<ul>
												@foreach($post->categories as $category)
													<li>{{ $category->name }}</li>
												@endforeach
											</ul>
											@endif
										</td>
										<td>
											@if($post->tags)
											<ul>
												@foreach($post->tags as $tag)
													<li>{{ $tag->name }}</li>
												@endforeach		
											</ul>
											@endif
										</td>
										<td>
											<img src="{{ $post->attachment ? $post->attachment->file : 'https://dummyimage.com/200x200/' }}" alt="" width="100" class="img-responsive img-rounded" />
										</td>
										<td>{{ $post->created_at->format('Y-m-d H:i') }}</td>
									</tr>
								@endforeach
							@endif

						</tbody>
					</table>
				</div>
			</div>

			@if ( $posts->perPage() < $posts->total() )
			<div class="row">
				<div class="col-sm-12 col-md-5">
					<div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing 1 to {{ $posts->perPage() }} of {{ $posts->total() }} entries</div>
				</div>
				<div class="col-sm-12 col-md-7">
					<div class="dataTables_paginate paging_simple_numbers pull-right" id="example2_paginate">
						{{ $posts->render() }}
					</div>
				</div>
			</div>
			@endif

		</div>
	</div>
	<!-- /.card-body -->
</div>

@endsection