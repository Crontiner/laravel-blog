@extends('admin.layouts.index')

@section('page_title', 'Edit Post')
@section('body_class', 'posts-page edit')

@section('content')


	{!! Form::model($post, [
					'method' => 'PATCH', 
					'action' => ['AdminPostsController@update', $post->id], 
					'class' => 'col-md-6',
					'files' => TRUE,
				]) 
	!!}

		<div class="form-group">
			{!! Form::label('title', 'Title:') !!}
			{!! Form::text('title', null, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::submit('Update post', ['class' => 'btn btn-primary']) !!}
		</div>

	{!! Form::close() !!}


	@include('admin.layouts.form_error')


@endsection