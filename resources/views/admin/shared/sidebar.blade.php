<?php 
  $segments = array(); // "" || "categories" || "news"

  for ($i=0; $i <= 10; $i++) {
    if ( Request::segment($i) !== NULL ) {
      $segments []= Request::segment($i);
    }
  }

  $segment = implode('-', $segments);
  //echo '<p class="pull-right">'. $segment .'</p>';
?>


<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
<!-- Brand Logo -->
<a href="#" class="brand-link">
	<img src="{{ asset('public/admin/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
	  style="opacity: .8">
	<span class="brand-text font-weight-light">AdminLTE 3</span>
</a>

<!-- Sidebar -->
<div class="sidebar">

  <!-- Sidebar Menu -->
  <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">


      <li class="nav-item">
        <a href="{{ route('front-page') }}" class="nav-link">
          <i class="nav-icon fa fa-home"></i>
          <p>Visit Site</p>
        </a>
      </li>


      <li class="nav-item">
        <a href="{{ route('admin') }}" class="nav-link @if($segment == 'admin') active @endif">
          <i class="nav-icon fa fa-dashboard"></i>
          <p>Dashboard</p>
        </a>
      </li>


      {{-- Media dropdown --}}

      <li class="nav-item has-treeview @if(strpos($segment, 'admin-media') !== false) menu-open @endif">
        <a href="#" class="nav-link">
          <i class="nav-icon fa fa-picture-o"></i>
          <p>Media <i class="fa fa-angle-left right"></i></p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="#" class="nav-link @if($segment == 'admin-media') active @endif">
              <i class="fa fa-list-ul nav-icon"></i>
              <p>Library</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link nav-link @if($segment == 'admin-media-create') active @endif">
              <i class="fa fa-plus nav-icon"></i>
              <p>Add New</p>
            </a>
          </li>
        </ul>
      </li>


      {{-- Posts dropdown --}}

      <li class="nav-item has-treeview @if(strpos($segment, 'admin-posts') !== false) menu-open @endif">
        <a href="#" class="nav-link">
          <i class="nav-icon fa fa-thumb-tack"></i>
          <p>Posts <i class="fa fa-angle-left right"></i></p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="{{ route('posts.index') }}" class="nav-link @if($segment == 'admin-posts') active @endif">
              <i class="fa fa-list-ul nav-icon"></i>
              <p>All Posts</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('posts.create') }}" class="nav-link nav-link @if($segment == 'admin-posts-create') active @endif">
              <i class="fa fa-plus nav-icon"></i>
              <p>Add New</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('posts-categories.index') }}" class="nav-link @if(strpos($segment, 'admin-posts-categories') !== false) active @endif">
              <i class="fa fa-list-ul nav-icon"></i>
              <p>Categories</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('posts-tags.index') }}" class="nav-link @if($segment == 'admin-posts-tags') active @endif">
              <i class="fa fa-list-ul nav-icon"></i>
              <p>Tags</p>
            </a>
          </li>
        </ul>
      </li>


      {{-- Users dropdown --}}

      <li class="nav-item has-treeview @if(strpos($segment, 'admin-users') !== false) menu-open @endif">
        <a href="#" class="nav-link">
          <i class="nav-icon fa fa-user"></i>
          <p>Users <i class="fa fa-angle-left right"></i></p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="{{ route('users.index') }}" class="nav-link @if($segment == 'admin-users') active @endif">
              <i class="fa fa-list-ul nav-icon"></i>
              <p>All Users</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('users.create') }}" class="nav-link @if($segment == 'admin-users-create') active @endif">
              <i class="fa fa-plus nav-icon"></i>
              <p>Add New</p>
            </a>
          </li>
        </ul>
      </li>


    </ul>
  </nav>
  <!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
</aside>