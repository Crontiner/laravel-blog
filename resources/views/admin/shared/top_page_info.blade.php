@if(Session::has('deleted_user'))
<div class="callout callout-info">
	<p>{{ session('deleted_user') }}</p>
</div>
@endif

@if(Session::has('deleted_user_image'))
<div class="callout callout-danger">
	<p>{{ session('deleted_user_image') }}</p>
</div>
@endif