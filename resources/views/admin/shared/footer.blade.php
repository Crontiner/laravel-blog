    </div>
    <!-- ./wrapper -->



    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    <script src="{{ asset('public/admin/plugins/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap -->
    <script src="{{ asset('public/admin/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- AdminLTE App -->
    <script src="{{ asset('public/admin/dist/js/adminlte.js') }}"></script>

    <!-- OPTIONAL SCRIPTS -->
    <script src="{{ asset('public/admin/dist/js/demo.js') }}"></script>



    <!-- PAGE PLUGINS -->

    <!-- InputMask -->
    <script src="{{ asset('public/admin/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('public/admin/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('public/admin/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>

    <!-- SparkLine -->
    <script src="{{ asset('public/admin/plugins/sparkline/jquery.sparkline.min.js') }}"></script>

    <!-- jVectorMap -->
    <script src="{{ asset('public/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ asset('public/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>

    <!-- SlimScroll 1.3.0 -->
    <script src="{{ asset('public/admin/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>

    <!-- ChartJS 1.0.2 -->
    <script src="{{ asset('public/admin/plugins/chartjs-old/Chart.min.js') }}"></script>

    <!-- Timepicker -->
    <script src="{{ asset('public/admin/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>

    <!-- Dropzone -->
    <script src="{{ asset('public/admin/plugins/dropzone/min/dropzone.min.js') }}"></script>



    <!-- PAGE SCRIPTS -->

    <script src="{{ asset('public/admin/dist/js/pages/dashboard2.js') }}"></script>
    <script src="{{ asset('public/admin/admin.js') }}"></script>

  </body>
</html>