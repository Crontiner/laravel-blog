<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Laravel Blog Admin</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('public/admin/plugins/font-awesome/css/font-awesome.min.css') }}">

    <!-- Timepicker -->
    <link rel="stylesheet" href="{{ asset('public/admin/plugins/timepicker/bootstrap-timepicker.min.css') }}">

    <!-- Dropzone -->
    <link rel="stylesheet" href="{{ asset('public/admin/plugins/dropzone/min/basic.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/admin/plugins/dropzone/min/dropzone.min.css') }}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('public/admin/dist/css/adminlte.min.css') }}">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="{{ asset('public/admin/admin.css') }}" />

  </head>
  <body class="hold-transition sidebar-mini admin @yield('body_class')">
    <div class="wrapper">

      <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
          </li>
        </ul>
      </nav>