@include('admin.shared.header')
@include('admin.shared.sidebar')

<div class="content-wrapper">
  @include('admin.shared.top_nav')

  <section class="content">
    <div class="container-fluid">

      @include('admin.shared.top_page_info')

      @yield('content')

    </div>
  </section>
</div>

<aside class="control-sidebar control-sidebar-dark"></aside>

@include('admin.shared.footer')